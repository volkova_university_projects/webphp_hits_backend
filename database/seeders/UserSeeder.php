<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                ['username' => 'akimov', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'anikushin.roma', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Cat12345', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'survivor85', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'striaaaang', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'user2323', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'leonidTestUsername', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'fanot', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'username13', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'string32', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'la_la_la', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'DimaMartyshev', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Kitty', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'A1', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'fa', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'canon', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'string12', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'KeMiliUs', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Ivanqwert1', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'wellwellwell', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'mylog1', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'mylog', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Hyun-jin', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'dobryj_chel', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'KristinaNaumova', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'kisus', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'polupoker', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Test123', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'admin', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'vio', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Rtx', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'happy_piece', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'ivanov_ii', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Emil', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'e', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'ar', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'ziz', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'bigpablo', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'love_react', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'qwe', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'nigger', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'string', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'xxx_cool_kitten_xxx', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'furlupe', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'krig', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'snitch', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'macoolri', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'aassdd', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'musaev', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'Cortuzz', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'snusevich', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'maksim', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
                ['username' => 'igor', 'password' => 'testtesttest', 'email' => 'root@lol.ke', ],
            ]
        );
    }
}
