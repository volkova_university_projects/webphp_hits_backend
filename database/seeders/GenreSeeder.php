<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert(
            [
                ['sid' => '3b781828-f329-4785-aecd-08d9b9f3d27c', 'name' => 'комедия'],
                ['sid' => 'b4e44ca7-7b96-4d12-aed3-08d9b9f3d27c', 'name' => 'мелодрама'],
                ['sid' => '5fb99899-1833-4e9e-aed7-08d9b9f3d27c', 'name' => 'военный'],
                ['sid' => '279e470a-11dc-46e2-aeda-08d9b9f3d27c', 'name' => 'история'],
                ['sid' => '9bba1883-a1fc-448a-aedb-08d9b9f3d27c', 'name' => 'драма'],
                ['sid' => 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c', 'name' => 'мультфильм'],
                ['sid' => '81637220-a847-4c81-aed5-08d9b9f3d27c', 'name' => 'приключения'],
                ['sid' => '5cb61466-df81-4a75-aed6-08d9b9f3d27c', 'name' => 'фэнтези'],
                ['sid' => '4186b674-c6df-49e4-aed8-08d9b9f3d27c', 'name' => 'семейный'],
                ['sid' => 'bda8f692-2883-42ef-aed9-08d9b9f3d27c', 'name' => 'аниме'],
                ['sid' => '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c', 'name' => 'детектив'],
                ['sid' => 'c7fa2d2b-a387-44bb-aedf-08d9b9f3d27c', 'name' => 'биография'],
                ['sid' => '511f9231-fa93-47cf-aede-08d9b9f3d27c', 'name' => 'криминал'],
                ['sid' => 'c2125424-47d4-4a19-aed0-08d9b9f3d27c', 'name' => 'фантастика'],
                ['sid' => 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c', 'name' => 'боевик'],
                ['sid' => 'cb961ace-1005-4077-aee4-08d9b9f3d27c', 'name' => 'мюзикл'],
                ['sid' => 'de11123e-3908-47eb-aed1-08d9b9f3d27c', 'name' => 'триллер'],
            ]
        );
    }
}
