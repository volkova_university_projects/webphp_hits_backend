<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;
use App\Models\Genre;

class MovieGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movie_genres')->insert(
            [
                ['movie_id' =>  Movie::query()->where('sid','=', 'b6c5228b-91fb-43a1-a2ac-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b6c5228b-91fb-43a1-a2ac-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'b4e44ca7-7b96-4d12-aed3-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b6c5228b-91fb-43a1-a2ac-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5fb99899-1833-4e9e-aed7-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b6c5228b-91fb-43a1-a2ac-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '279e470a-11dc-46e2-aeda-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b6c5228b-91fb-43a1-a2ac-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '82c34463-daf4-4702-a2b8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '82c34463-daf4-4702-a2b8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '82c34463-daf4-4702-a2b8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '82c34463-daf4-4702-a2b8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '4186b674-c6df-49e4-aed8-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '82c34463-daf4-4702-a2b8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'bda8f692-2883-42ef-aed9-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7eae1e59-d6c5-4316-a2af-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7eae1e59-d6c5-4316-a2af-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7eae1e59-d6c5-4316-a2af-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7eae1e59-d6c5-4316-a2af-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7eae1e59-d6c5-4316-a2af-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '4186b674-c6df-49e4-aed8-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '988717c3-bd0f-48f1-a2b3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5fb99899-1833-4e9e-aed7-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '988717c3-bd0f-48f1-a2b3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '279e470a-11dc-46e2-aeda-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '988717c3-bd0f-48f1-a2b3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '988717c3-bd0f-48f1-a2b3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c7fa2d2b-a387-44bb-aedf-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d50d333-73fd-4c8a-a2bb-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d50d333-73fd-4c8a-a2bb-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d50d333-73fd-4c8a-a2bb-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '22158c42-001a-40a3-a2a7-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '388cd8ec-5cb7-4e8b-a2ae-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '388cd8ec-5cb7-4e8b-a2ae-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '388cd8ec-5cb7-4e8b-a2ae-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b2ae5845-ff03-489d-a2a6-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'b2ae5845-ff03-489d-a2a6-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '05b3f644-d8ac-41d5-a2ba-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '05b3f644-d8ac-41d5-a2ba-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '05b3f644-d8ac-41d5-a2ba-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '05b3f644-d8ac-41d5-a2ba-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '11253dbf-88b6-4149-a2c3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '11253dbf-88b6-4149-a2c3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '11253dbf-88b6-4149-a2c3-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '1cb0d839-2030-434d-a2a9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '1cb0d839-2030-434d-a2a9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '1cb0d839-2030-434d-a2a9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f233bcdc-94d2-4345-a2a8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f233bcdc-94d2-4345-a2a8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f233bcdc-94d2-4345-a2a8-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '544dd762-3028-4e99-a2aa-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '544dd762-3028-4e99-a2aa-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '544dd762-3028-4e99-a2aa-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'a1748020-fd86-45a0-a2b2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'a1748020-fd86-45a0-a2b2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'a1748020-fd86-45a0-a2b2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'a1748020-fd86-45a0-a2b2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '4186b674-c6df-49e4-aed8-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d2b93f0-4e13-4bbb-a2c0-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d2b93f0-4e13-4bbb-a2c0-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5fb99899-1833-4e9e-aed7-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5d2b93f0-4e13-4bbb-a2c0-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '2ba1b498-a22f-497d-a2c2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '2ba1b498-a22f-497d-a2c2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '2ba1b498-a22f-497d-a2c2-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f7c6a32b-a55b-4d86-a2bd-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f7c6a32b-a55b-4d86-a2bd-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'b4e44ca7-7b96-4d12-aed3-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f7c6a32b-a55b-4d86-a2bd-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'c354f185-220a-425b-a2b1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'c354f185-220a-425b-a2b1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'c354f185-220a-425b-a2b1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c7fa2d2b-a387-44bb-aedf-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5765a388-7ccd-4560-a2b6-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5765a388-7ccd-4560-a2b6-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7ac521e0-4fa3-4f20-a2b0-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '7ac521e0-4fa3-4f20-a2b0-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5181f3ac-dd81-4105-a2ab-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5181f3ac-dd81-4105-a2ab-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5181f3ac-dd81-4105-a2ab-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '4186b674-c6df-49e4-aed8-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5181f3ac-dd81-4105-a2ab-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '5181f3ac-dd81-4105-a2ab-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'cb961ace-1005-4077-aee4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '9ebb5ed4-ec16-424a-a2b7-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '9ebb5ed4-ec16-424a-a2b7-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'be8d9187-95c6-4cb9-aece-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '9ebb5ed4-ec16-424a-a2b7-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '9ebb5ed4-ec16-424a-a2b7-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '4186b674-c6df-49e4-aed8-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '6e62bde4-34c6-45d1-a2b5-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '6e62bde4-34c6-45d1-a2b5-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '6e62bde4-34c6-45d1-a2b5-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '819ff720-1dc0-4d32-a2ad-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '819ff720-1dc0-4d32-a2ad-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '819ff720-1dc0-4d32-a2ad-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '27e0d4f4-6e31-4053-a2be-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '27e0d4f4-6e31-4053-a2be-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '27e0d4f4-6e31-4053-a2be-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '5cb61466-df81-4a75-aed6-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'bae324ff-f0e8-40a4-a2c1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'de11123e-3908-47eb-aed1-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'bae324ff-f0e8-40a4-a2c1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'bae324ff-f0e8-40a4-a2c1-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '0a76bd64-ccbb-42cb-a2bf-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '0a76bd64-ccbb-42cb-a2bf-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'b4e44ca7-7b96-4d12-aed3-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '0a76bd64-ccbb-42cb-a2bf-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '511f9231-fa93-47cf-aede-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'db81da99-4fa2-4358-a2b4-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'db81da99-4fa2-4358-a2b4-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'de11123e-3908-47eb-aed1-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'db81da99-4fa2-4358-a2b4-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'f676d64d-b6c7-4a74-aed2-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'db81da99-4fa2-4358-a2b4-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '8f9b6c11-cfd8-4810-aed4-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'db81da99-4fa2-4358-a2b4-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f2c3eda9-ca6e-4db5-a2b9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f2c3eda9-ca6e-4db5-a2b9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'c2125424-47d4-4a19-aed0-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', 'f2c3eda9-ca6e-4db5-a2b9-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '81637220-a847-4c81-aed5-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '77ca3126-bace-49e9-a2bc-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '3b781828-f329-4785-aecd-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '77ca3126-bace-49e9-a2bc-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', 'b4e44ca7-7b96-4d12-aed3-08d9b9f3d27c')->firstOrFail()->int_id, ],
                ['movie_id' =>  Movie::query()->where('sid','=', '77ca3126-bace-49e9-a2bc-08d9b9f3d2a2')->firstOrFail()->int_id, 'genre_id' =>  Genre::query()->where('sid','=', '9bba1883-a1fc-448a-aedb-08d9b9f3d27c')->firstOrFail()->int_id, ],
            ]
        );
    }
}
