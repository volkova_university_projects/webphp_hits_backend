<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id('int_id');
            $table->uuid('sid');
            $table->string('name')->nullable(true);
            $table->string('poster')->nullable(true);
            $table->integer('year');
            $table->string('country')->nullable(true);
            $table->integer('time');
            $table->string('tagline')->nullable(true);
            $table->string('description')->nullable(true);
            $table->string('director')->nullable(true);
            $table->integer('budget')->nullable(true);
            $table->integer('fees')->nullable(true);
            $table->integer('ageLimit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
};
