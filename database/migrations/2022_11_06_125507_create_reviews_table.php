<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id('int_id');
            $table->uuid('sid');
            $table->integer('rating');
            $table->string('reviewText')->nullable(true);
            $table->boolean('isAnonymous');
            $table->dateTime('createDateTime');
            $table->foreignId('movie_id')->references('int_id')->on('movies')->onDelete('cascade');
            $table->foreignId('user_id')->references('int_id')->on('profiles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
};
