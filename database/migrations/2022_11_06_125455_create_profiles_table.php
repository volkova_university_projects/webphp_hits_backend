<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->foreignId('int_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('sid');
            $table->string('userName')->nullable(true)->unique();
            $table->string('nickName')->nullable(true);
            $table->string('password');
            $table->string('email');
            $table->string('avatarLink')->nullable(true);
            $table->string('name');
            $table->dateTime('birthDate');
            $table->enum('gender', [0,1]);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
