<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FavoriteController;
use App\Http\Controllers\Api\GenreController;
use App\Http\Controllers\Api\MovieController;
use App\Http\Controllers\Api\MovieGenresController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\ReviewController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Эндпионты доступны только для авторизованных пользователей
Route::middleware('auth:api')->group(function () {

    Route::apiResource('/genres', GenreController::class);

    Route::get('/account/profile', [ProfileController::class, 'show']);
    Route::put('/account/profile', [ProfileController::class, 'update']);

    Route::get('/favorites', [FavoriteController::class, 'index']);
    Route::post('/favorites/{movieId}/add', [FavoriteController::class, 'store']);
    Route::delete('/favorites/{movieId}/delete', [FavoriteController::class, 'destroy']);

    Route::post('/movie/{movieId}/review/add', [ReviewController::class, 'store']);
    Route::put('/movie/{movieId}/review/{id}/edit', [ReviewController::class, 'update']);
    Route::delete('/movie/{movieId}/review/{id}/delete', [ReviewController::class, 'destroy']);

    Route::post('/account/logout', [AuthController::class, 'logoutUser']);

    Route::post('/movies_genres/{movieId}', [MovieGenresController::class, 'store']);
    Route::get('/movies_genres/{movieId}', [MovieGenresController::class, 'show']);
    Route::get('/movies_genres/', [MovieGenresController::class, 'index']);

});


Route::controller(AuthController::class)->group(function () {
    Route::post('/account/register', 'createUser');
    Route::post('/account/login', 'loginUser');
});

Route::get('/movies/details/{movieId}', [MovieController::class, 'show']);

Route::apiResource('/movies/{page?}', MovieController::class)->only(['index','store','update']);
