<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReviewShortCollection extends ResourceCollection
{
    /**
     * Коллекция отзывов с краткой информацией
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
           ReviewShortResource::collection($this),
        ];
    }
}
