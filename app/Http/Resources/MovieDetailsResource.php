<?php

namespace App\Http\Resources;

use App\Models\MovieGenres;
use App\Models\Review;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieDetailsResource extends JsonResource
{
    /**
     * Подробная информация о фильме с подробной информацией об отзывах
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->sid,
            'name'=>$this->name,
            'poster'=>$this->poster,
            'year'=>$this->year,
            'country'=>$this->country,
            'genres'=>MovieGenres::query()->where('movie_id','=', $this->int_id)->get(),
            'reviews'=>new ReviewCollection(Review::query()->where('movie_id','=', $this->int_id)->get()),
            'time'=>$this->time,
            'tagline'=>$this->tagline,
            'description'=>$this->description,
            'director'=>$this->director,
            'budget'=>$this->budget,
            'fees'=>$this->fees,
            'ageLimit'=>$this->ageLimit,
        ];
    }
}
