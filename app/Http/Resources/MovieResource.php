<?php

namespace App\Http\Resources;

use App\Models\MovieGenres;
use App\Models\Review;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Краткая информация о фильме и отзывах
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->sid,
            'name'=>$this->name,
            'poster'=>$this->poster,
            'year'=>$this->year,
            'country'=>$this->country,
            'genres'=>MovieGenres::query()->where('movie_id','=', $this->int_id)->get(),
            'reviews'=>new ReviewShortCollection(Review::query()->where('movie_id','=', $this->int_id)->get()),
        ];
    }
}
