<?php

namespace App\Http\Resources;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{

     /** Подробная информация об отзыве
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "rating"=>$this->rating,
            "reviewText"=>$this->reviewText,
            "isAnonymous"=>$this->isAnonymous,
            "createDateTime"=>$this->createDateTime,
            "author"=>new UserShortResource(Profile::query()->where('int_id','=',$this->user_id)->firstOrFail()),
        ];
    }
}
