<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserShortResource extends JsonResource
{
    /**
     * Краткая информация о пользователе (для подробной информации об отзыве)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "userId"=>$this->sid,
            "nickName"=>$this->nickName,
            "avatar"=>$this->avatarLink,
        ];
    }
}
