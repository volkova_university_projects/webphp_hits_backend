<?php

namespace App\Http\Resources;

use App\Models\MovieGenres;
use App\Models\Review;
use App\Models\Movie;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MovieCollection extends JsonResource
{
    /**
     * Коллекция списка фильмов с кратким описанием каждого фильма
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return MovieResource::collection($this);
    }
}
