<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieGenres;
use Illuminate\Http\Request;

/** Класс для служебного эндпоинта работы с привязкой жанров к фильмам, реализует crud,
 * Не завершен
 */
class MovieGenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $movie_genres = MovieGenres::query()->get();

        return response()->json($movie_genres->all(), 200, ['Content-type'=>'text/json']);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie_id = Movie::query()->where('sid','=',$request->movieId)->get()->firstOrFail()->int_id;
        MovieGenres::query()->where('movie_id','=',$movie_id)->delete();
        foreach ($request->genres as $genreId){
            $genre_id = Genre::query()->where('sid','=',$genreId)->get()->firstOrFail()->int_id;
            MovieGenres::create([
                'movie_id' => $movie_id,
                'genre_id' => $genre_id,
            ]);
        }
        return response()->noContent(201, ['Content-type'=>'text/plain']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MovieGenres  $movieGenres
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, MovieGenres $movieGenres)
    {
        if (empty($request->id)){
            return $this->index($request);
        }
        $movie_genres = MovieGenres::query()->where('id','=',$request->id)->get();
        return response()->json($movie_genres, 200, ['Content-type'=>'text/json']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MovieGenres  $movieGenres
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovieGenres $movieGenres)
    {
        MovieGenres::query()->where('movie_id','=',$request->movieId)->delete();
        foreach ($request->genres as $genreId){
            MovieGenres::create([
                'movie_id' => Movie::query()->where('sid','=',$request->movieId)->get()->first()->int_id,
                'genre_id' => Genre::query()->where('sid','=',$genreId)->get()->first()->int_id,
            ]);
        }
        return response()->noContent(200, ['Content-type'=>'text/plain']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MovieGenres  $movieGenres
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, MovieGenres $movieGenres)
    {
        MovieGenres::query()->where('movie_id','=',$request->movieId)->delete();
        return response()->noContent(204, ['Content-type'=>'text/plain']);
    }
}
