<?php

namespace App\Http\Controllers\Api;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{



    public function createUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'userName' => 'required|unique:App\Models\User,username',
                    'name' => 'required',
                    'password' => 'required',
                    'email' => 'required|email',
                    'birthDate' => 'date',
                    'gender' => 'integer|between:0,1',
                ]);

            if($validateUser->fails()){
                return response()->json([
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 400);
            }

            $user = new User([
                'username' => $request->userName,
                'password' => Hash::make($request->password),
                'email' => $request->email,
            ]);
            $user->save();
            // создание профиля пользователя
            //TODO: заменить на корректную работу с ORM в стиле Eloquent
            $profile = new Profile([
                'int_id' => $user->id,
                'sid' => uuid_create(),
                'userName' => $request->userName,
                'name' => $request->name,
                'password' => Hash::make($request->password),
                'email' => $request->email,
                'birthDate' => date_create($request->birthDate),
                'gender' => str($request->gender),
            ]);
            $profile->save();
            //выписываем токен при регистрации
            $credentials = $request->only('userName', 'password');
            $token = Auth::attempt($credentials);
            $user = Auth::user();
            return response()->json([
                'token' => $token
            ], 200);

        // если что то пошло не так, то статус 500
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function loginUser(Request $request)
    {
        try { //проверяем заполнены ли поля, иначе 400 статус
            $validateUser = Validator::make($request->all(),
                [
                    'username' => 'required',
                    'password' => 'required'
                ]);

            if($validateUser->fails()){
                return response()->noContent(400);
            }

            $credentials = $request->only('username', 'password');
            // пытаемся войти, если не удалось http 400
            $token = Auth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'message' => 'Login failed',
                ], 400);
            }
            // возвращаем токен JWT
            $user = Auth::user();
            return response()->json([
                'token' => $token,
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function logoutUser(Request $request){
        Auth::logout();
        return response()->noContent(200);
    }
}
