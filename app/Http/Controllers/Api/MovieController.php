<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovieRequest;
use App\Http\Resources\MovieDetailsResource;
use App\Models\Movie;
use App\Http\Resources\MovieCollection;
use Illuminate\Http\Request;


class MovieController extends Controller
{
    /**
     * Отображает список фильмов с разбивкой по страницам
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($page = 1)
    {
        $page = max(intval($page), 1);
        $pageSize = 10;

        $query = Movie::query();
        $pageCount = ceil(($query->count()) / $pageSize);
        $movies = $query->offset(($page - 1) * $pageSize)->limit($pageSize)->get();

        $pageInfo = [
            'pageSize'=>$pageSize,
            'pageCount'=>$pageCount,
            'currentPage'=>$page,
        ];

        return response()->json(
            [
                "movies"=>new MovieCollection($movies), // возвращаем коллекцию списка фильмов
                'pageInfo'=>$pageInfo,
            ]
        );

    }


    /**
     * Отображение полной информации о фильме
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Movie $movie)
    {
        return response()->json(new MovieDetailsResource(
            Movie::query()
                ->where('sid','=',$request->movieId)
                ->first()));
    }

}
