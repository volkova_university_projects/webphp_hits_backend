<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGenreRequest;
use App\Models\Genre;
use Illuminate\Http\Request;

/** Класс для служебного эндпоинта работы со списком жанров, реализует crud
 *
 */
class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $genres = Genre::all();
        return response()->json($genres);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGenreRequest $request)
    {
        $request->validate($request->rules());
        $genre = new Genre;
        $genre->name = $request->name;
        $genre->sid = uuid_create();
        $genre->save();

        return response()->json([
            'status' => true,
            'message' => "Genre Created successfully!",
            'genre' => $genre
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Genre $genre)
    {
        $request->validate($request->rules());
        $genre = update($request->all());
        return response()->json([
            'status' => true,
            'message' => "Genre Updated successfully!",
            'genre' => $genre
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Genre $genre)
    {
        $genre = delete();
        return response()->json([
            'status' => true,
            'message' => "Genre Deleted successfully!",
            'genre' => $genre
        ], 200);
    }
}
