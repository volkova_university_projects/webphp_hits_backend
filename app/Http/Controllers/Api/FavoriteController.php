<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MovieCollection;
use App\Models\Favorite;
use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FavoriteController extends Controller
{
    /**
     * отображаем список избранного для пользователя
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $movies = Movie::query()
            ->whereIn(
                'int_id',
                Favorite::query()  // подзапрос для получения списка id избранных фильмов
                    ->where(
                        'user_id',
                        '=',
                        $request->user()->id
                    )
                    ->get(['movie_id'])
            )->get();

        return response()->json(['movies'=>new MovieCollection($movies)]); // возвращаем коллекцию списка фильмов
    }

    /**
     * Функция добавления фильма в избранное
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Favorite::create([
            'movie_id' => Movie::query()
                ->where('sid','=',$request->movieId)
                ->get()->firstOrFail()->int_id,
            'user_id' => $request->user()->id
        ]);
        return response()->noContent(200, ['Content-type'=>'text/plain']);
    }

    /**
     * удаление фильма из избранного
     *
     * @param  \App\Models\Favorite  $favorite
     * @return Response
     */
    public function destroy(Request $request,Favorite $favorite)
    {
        $movie_id = Movie::query()->where('sid','=',$request->movieId)->get()->firstOrFail()->int_id;
        Favorite::query()
                    ->where(
                        'user_id',
                        '=',
                        $request->user()->id
                    )->where('movie_id','=',$movie_id)->delete();
        return response()->noContent(200, ['Content-Type'=>'text/plain']);
    }
}
