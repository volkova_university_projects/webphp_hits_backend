<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProfileController extends Controller
{
    /**
     * Отображаем профиль текущего пользователя
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $profile = Profile::where('int_id', '=', $request->user()->id)->first();
        return response()->json($profile);
    }

    /**
     * Редактирование профиля текущего пользователя
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        $profile = Profile::query()->where('int_id', '=', $request->user()->id)->first();
        DB::table('profiles')
            ->where('sid','=',$profile->sid)
            ->update([
                'nickName' => $request->nickName,
                'email' => $request->email,
                'avatarLink' => $request->avatarLink,
                'name' => $request->name,
                'birthDate' => date_create($request->birthDate),
                'gender' => isset($request->gender) ? str($request->gender) : str($profile->gender), // не меняем гендер если пользователь этого не просил
            ]);

        $user = User::where('id','=',$request->user()->id);
        $user->update([
           'email' => $request->email,
        ]);

        return response()
            ->noContent(200)
            ->withHeaders([
                'Content-Type' => 'text/plain'
            ]);
    }

}
