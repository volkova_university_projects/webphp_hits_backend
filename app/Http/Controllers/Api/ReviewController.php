<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReviewRequest;
use App\Models\Movie;
use App\Models\Profile;
use App\Models\Review;
use Illuminate\Http\Request;


class ReviewController extends Controller
{

    /**
     * Создание отзыва о фильме
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreReviewRequest $request)
    {
        $review = new Review($request->all());
        $review->sid = uuid_create();
        $review->createDateTime = now();
        $review->movie_id = Movie::query()->where('sid','=', $request->movieId)->firstOrFail()->int_id;
        $review->user_id = Profile::where('int_id', '=', $request->user()->id)->first()->int_id;
        $review->save();
        return response()->noContent( 200, ['Content-Type'=>'text/plain']);
    }

    /**
     * Редактирование отзыва
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(StoreReviewRequest $request, Review $review)
    {
        $movie_id = Movie::query()
            ->where('sid', '=', $request->movieId)
            ->get()
            ->first()
            ->int_id;
        $review_id = Review::query()
            ->where('movie_id', '=', $movie_id)
            ->where('user_id', '=', $request->user()->id) //не даем пользователю редеактировать чужие отзывы
            ->where('sid','=',$request->id);
        if ($review_id->count() == 0){
            return response()->noContent(404, ['Content-Type'=>'text/plain']);
        }
        $review_id->update($request->all());
        return response()->noContent(200, ['Content-Type'=>'text/plain']);
    }

    /**
     * Удаление отзыва
     *
     * @param  \App\Models\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Review $review)
    {
        $movie_id = Movie::query()
            ->where('sid', '=', $request->movieId)
            ->get()
            ->first()
            ->int_id;
        $review_id = Review::query()
            ->where('movie_id', '=', $movie_id)
            ->where('user_id', '=', $request->user()->id)
            ->where('sid','=',$request->id);
        if ($review_id->count() == 0){
            return response()->noContent(404, ['Content-Type'=>'text/plain']);
        }
        $review_id->delete();
        return response()->noContent(200, ['Content-Type'=>'text/plain']);
    }
}
