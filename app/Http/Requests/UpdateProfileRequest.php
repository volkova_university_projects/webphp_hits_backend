<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => 'string',
            'nickName' => 'string|nullable',
            'email' => 'required|email',
            'avatarLink' => 'string|nullable',
            'name' => 'required|string',
            'birthDate' => 'required|date',
            'gender' => 'integer|between:0,1',
        ];
    }
}
