<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovieGenres extends Model
{
    use HasFactory;

    protected $fillable = ['movie_id', 'genre_id'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Метод возращающий данные о жанрах для фильма в кратком виде
     * @return array
     */
    public function toArray()
    {
        return [
            'id'=>Genre::query()->where('int_id','=',$this->genre_id)->first()->sid,
            'name'=>Genre::query()->where('int_id','=',$this->genre_id)->first()->name,
        ];
    }


}
