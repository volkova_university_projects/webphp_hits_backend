<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = ['reviewText', 'rating', 'isAnonymous'];

    protected $hidden = ['int_id', 'created_at', 'updated_at', 'sid'];

    protected $appends = ['id'];

    public function getIdAttribute(){
        return $this->attributes['sid'];
    }

}
