<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Profile extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['nickName', 'email', 'password', 'userName',
        'avatarLink', 'name', 'birthDate', 'gender', 'sid', 'int_id', 'id'];

    protected $hidden = ['int_id', 'created_at', 'updated_at', 'sid', 'password', 'remember_token'];


    protected $appends = ['id'];

    public function getIdAttribute(){
        return $this->attributes['sid'];
    }
}
