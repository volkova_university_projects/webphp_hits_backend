<?php

namespace App\Models;

use App\Http\Resources\MovieCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Movie extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'poster', 'year', 'country', 'time',
        'tagline', 'description', 'director', 'budget', 'fees', 'ageLimit'];


    protected $hidden = ['int_id', 'created_at', 'updated_at', 'sid'];

    protected $appends = ['id'];

    public function getIdAttribute(){
        return $this->attributes['sid'];
    }
}
